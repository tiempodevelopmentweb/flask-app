# Observatory Service

# Import framework
from flask import Flask
from flask_restful import Resource, Api
from resources.observatory import Observatory
from resources.users import UsersList

# Instantiate the app
app = Flask(__name__)
api = Api(app)

# Create routes
api.add_resource(Observatory, '/')
api.add_resource(UsersList, '/users')

# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
