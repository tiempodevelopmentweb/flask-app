from flask_restful import Resource, Api

users = [
    {
        'id': 1,
        'name': 'Ada Lovelace',
        'email': 'ada@lovelace.com',
    },
    {
        'id': 2,
        'name': 'Dennis Ritchie',
        'email': 'dennis@ritchie.com',
    },
    {
        'id': 3,
        'name': 'Danielle Bunten Berry',
        'email': 'dbunten@berry.org',
    },
    {
        'id': 4,
        'name': 'Alan Kay',
        'email': 'alan.kay@colorado.edu',
    },

]

class UsersList(Resource):
    def get(self):
        return {
            'users': users
        }
