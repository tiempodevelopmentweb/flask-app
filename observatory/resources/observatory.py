from flask_restful import Resource, Api

class Observatory(Resource):
    def get(self):
        return {
            'Galaxies': [
                'Milkyway',
                'Andromeda',
                'Large Magellanic Cloud (LMC)',
                'Small Magellanic Cloud (SMC)',
                'Test',
            ]
        }
