Python-Flask application
===================

Hey! this is our second application in our workshop, this one has 2 different containers that are going to be talking to get data from one place to another.

The application has 2 main endpoints:

http://host:5001/ <-- that returns data from an observatory 
http://host:5001/users <-- returns a list of users.

Where host is the IP address assigned to your container.

In this moment you can access the application from port 5000 which is a php app running over a apache webserver.

In order to setup the application you have to run this commands: 

docker commands
-------------

```
# Create the virtual machine
docker-machine create --driver virtualbox ws-python  

# point our environment to our vm
eval $(docker-machine env ws-python) 

# build the docker image
docker-compose build 

# initiate our service
docker-compose up 
```

During the session, we are going to review the project and also try to connect a third container into the mix. 

So you can see how its done.
